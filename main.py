from flask import Flask
from flask import jsonify

app = Flask(__name__)

@app.route("/")
def hello_world():
    return 'Hello, World'

@app.route("/data")
def data():
    import random
    response = []
    for x in range(25):
        response.append({"jam":x+1,"nilai":random.randint(20,41)})
    return jsonify(response)
if __name__ == "__main__":
    app.run(host="0.0.0.0")